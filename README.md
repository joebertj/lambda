# lambda

An AWS Lambda, API Gateway, Terraform, IAM, DynamoDB, S3, Claudia, Node.js, Gitlab CI test
## Prerequisites
`terraform`  
`aws cli`  
`npm`  
`jq`  
AWS Account with Admin IAM to generate the claudia profile's aws credentials  
Gitlab Account for Repo and CI  

## Setup
```
npm install claudia -g  
npm install claudia-api-builder --save  
npm install aws-sdk --save  
```

### AWS CLI
`aws configure`  

Sample `~/.aws/credentials`  
```
[claudia]
aws_access_key_id = YOUR_ACCESS_KEY
aws_secret_access_key = YOUR_ACCESS_SECRET
```
### Terraform
```
cd terraform 
terraform init  
terraform validate  
terraform plan  
terraform apply  
```
Note: S3 is not a practical solution for saving code. A repo should be used instead. It's only here for completeness. The file app.js is being sent to an S3 bucket.  
Replace the claudia profile's aws credentials with the generated one from terraform/terraform.tfstate  
```
YOUR_ACCESS_KEY=$(jq '.modules[].resources["aws_iam_access_key.credentials"].primary.attributes.id' terraform.tfstate | sed "s/\"//g")
YOUR_ACCESS_SECRET=$(jq '.modules[].resources["aws_iam_access_key.credentials"].primary.attributes.secret' terraform.tfstate | sed "s/\"//g")
LINE=$(grep claudia -n ~/.aws/credentials | cut -d':' -f 1)
sed -i -e "$((LINE + 1))s/aws_access_key_id = .\+$/aws_access_key_id = $YOUR_ACCESS_KEY/" -e "$((LINE + 2))s/aws_secret_access_key = .\+$/aws_secret_access_key = $YOUR_ACCESS_SECRET/" ~/.aws/credentials
```
### Terraform (claudia profile)
```
cd claudia
terraform init
terraform validate
terraform plan
terraform apply
```

## Claudia
`claudia create --name claudia --region ap-northeast-1 --api-module app --policies policies --configure-db students --profile claudia`
### Error handling
`--name lambda` - Name your function uniqely (Function already exist)  
`--handler app.handler` - To test locally without using API gateway  
`--role lambda-executor` - If there was an error in the middle of create and role was already created, add this option to resume. (Role with name lambda-executor already exists)  
Use `claudia update` instead of `claudia create` (json already exists in the source folder)  

## CICD
- Make changes  
- Push the code to trigger a build  
`git commit -a`  
`git push`
View status on [Gitlab](https://gitlab.com/joebertj/lambda/pipelines)  
`.gitlab-ci.yaml` automated the build deploy process  
Job build: `npm start`  
Job deploy: `npm run deploy`  
Job test: 
```
APIID=$(jq .api.id claudia.json | sed "s/\"//g")
curl -H "Content-Type: application/json" -X POST --data @students.json https://$APIID.execute-api.ap-northeast-1.amazonaws.com/latest/import
curl -H "Content-Type: application/json" -X POST --data @student.json https://$APIID.execute-api.ap-northeast-1.amazonaws.com/latest/load
curl https://$APIID.execute-api.ap-northeast-1.amazonaws.com/latest/email/joebertj@gmail.com
```

## API Endpoints
/import - Accepts an array of students POST data in JSON format. See `students.json` for format. Uses batchwrite.  
/load - Accepts a student POST data in JSON format. See `student.json` for format.  
/email/{email} - Search for a student with email using GET  

### Manual Testing
```
npm run deploy
curl -H "Content-Type: application/json" -X POST --data @students.json https://c0u7pwnl9g.execute-api.ap-northeast-1.amazonaws.com/latest/import  
curl -H "Content-Type: application/json" -X POST --data @student.json https://c0u7pwnl9g.execute-api.ap-northeast-1.amazonaws.com/latest/load
aws dynamodb scan --table-name students  --profile claudia  
curl https://5n6lfruls1.execute-api.ap-northeast-1.amazonaws.com/latest/email/joebertj@gmail.com  
```

### Other npm commands
`npm install`  
`npm test`  

## Cleanup
```
claudia destroy --name claudia --profile claudia  
cd terraform/claudia  
terraform destroy  
cd ..
terraform destroy
```
