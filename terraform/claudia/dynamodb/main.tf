resource "aws_dynamodb_table" "students" {
  name           = "students"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "email"

  attribute {
    name = "email"
    type = "S"
  }

  tags = {
    Name        = "${var.name}"
    Environment = "${var.environment}"
  }
}
