variable "environment" {
  default     = "dev"
}

variable "name" {
  default     = "students"
}
