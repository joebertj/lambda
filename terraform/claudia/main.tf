provider "aws" {
  region  = "${var.region}"
  profile = "${var.profile}"
}

module "dynamodb" {
  source = "./dynamodb"
  name = "${var.dynamodb_name}"
  environment = "${var.environment}"
}

resource "aws_s3_bucket" "joebertj" {
  bucket = "joebertj"
  acl    = "private"
}

resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.joebertj.bucket}"
  key    = "app.js"
  source = "../../app.js"
}
