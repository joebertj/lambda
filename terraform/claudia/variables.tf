variable "region" {
  default     = "ap-northeast-1"
}

variable "profile" {
  default     = "claudia"
}

variable "environment" {
  default     = "dev"
}

variable "dynamodb_name" {
  default     = "students"
}
