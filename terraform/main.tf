provider "aws" {
  region  = "${var.region}"
  profile = "${var.profile}"
}

resource "aws_iam_user" "lambda" {
  name = "lambda"
}

resource "aws_iam_access_key" "credentials" {
  user    = "${aws_iam_user.lambda.name}"
}

resource "aws_iam_user_policy_attachment" "iam" {
  user       = "${aws_iam_user.lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMFullAccess"
}

resource "aws_iam_user_policy_attachment" "lambda" {
  user       = "${aws_iam_user.lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaFullAccess"
}

resource "aws_iam_user_policy_attachment" "api_gateway" {
  user       = "${aws_iam_user.lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonAPIGatewayAdministrator"
}

resource "aws_iam_user_policy_attachment" "dynamodb" {
  user       = "${aws_iam_user.lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_user_policy_attachment" "s3" {
  user       = "${aws_iam_user.lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

