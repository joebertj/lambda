/*global require, module*/
var ApiBuilder = require('claudia-api-builder'),
	AWS = require('aws-sdk'),
	api = new ApiBuilder(),
	dynamoDb = new AWS.DynamoDB.DocumentClient();

//exports.handler = api;
module.exports = api;

// Create new user
api.post('/load', function (request) {
	'use strict';
	var params = {
		TableName: request.env.tableName,
		Item: {
			email: request.body.email,
			lastName: request.body.lastName,
			firstName: request.body.firstName,
			age: request.body.age
		}
	};
	// return dynamo result directly
	return dynamoDb.put(params).promise().then(function (response) {
		return new ApiBuilder.ApiResponse({ "Records": 1, "Status": 200 }, {}, 200)
        });
}, {success: { contentType: 'application/json'}, error: 400}); // Return HTTP status 200 - Created when successful

// Create new user
api.post('/import', function (request) {
	'use strict';
	var s = request.body.Students;
	var students = []
	s.forEach(student => {
  	    students.push({
    		PutRequest: {
      		    Item: {
			email: student['email'],
			lastName: student['lastName'],
			firstName: student['firstName'],
			age: student['age']
      		    }
        	}
  	    });
	});

	let params = {
    	    RequestItems: {
        	'students': students
    	}
};
	// return dynamo result directly
	return dynamoDb.batchWrite(params).promise().then(function (response) {
		return new ApiBuilder.ApiResponse({ "Records": students.length, "Status": 200 }, {}, 200)
        });
}, {success: { contentType: 'application/json'}, error: 400}); // Return HTTP status 200 - Created when successful

// get user for {email}
api.get('/email/{email}', function (request) {
	'use strict';
	var email, params;
	// Get the email from the pathParams
	email = request.pathParams.email;
	params = {
		TableName: request.env.tableName,
		Key: {
			email: email
		}
	};

	// post-process dynamo result before returning
	return dynamoDb.get(params).promise().then(function (response) {
		return response.Item;
	});
});

// delete user with {email}
api.delete('/email/{email}', function (request) {
	'use strict';
	var email, params;
	// Get the email from the pathParams
	email = request.pathParams.email;
	params = {
		TableName: request.env.tableName,
		Key: {
			email: email
		}
	};
	// return a completely different result when dynamo completes
	return dynamoDb.delete(params).promise()
		.then(function () {
			return 'Deleted user with email "' + email + '"';
		});
}, {success: { contentType: 'text/plain'}});

api.addPostDeployConfig('tableName', 'DynamoDB Table Name:', 'configure-db');
